﻿public enum EMirrorType
{
    E_NO,
    E_LEFT,
    E_RIGHT
};

[System.Serializable]
public class Prompt
{
    public string WhichHand;
    public string LastHand;
    public float Angle;
    public float RestTime;
    public float PrepTime;
    public float MoveTime;
    public bool Mirror;
    public bool LastMirror;
    public bool Observe;

    public EMirrorType MirrorType;
    public EMirrorType LastMirrorType;

    public void Init()
    {
        if (Mirror)
        {
            switch (WhichHand)
            {
                case "Left":
                    MirrorType = EMirrorType.E_LEFT;
                    break;
                case "Right":
                    MirrorType = EMirrorType.E_RIGHT;
                    break;
                default:
                    MirrorType = EMirrorType.E_NO;
                    break;
            }
        }
        else
            MirrorType = EMirrorType.E_NO;

        if (!LastMirror) return;
        switch (LastHand)
        {
            case "Left":
                LastMirrorType = EMirrorType.E_LEFT;
                break;
            case "Right":
                LastMirrorType = EMirrorType.E_RIGHT;
                break;
            default:
                LastMirrorType = EMirrorType.E_NO;
                break;
        }
    }
};