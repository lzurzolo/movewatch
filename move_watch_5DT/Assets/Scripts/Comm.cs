﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class Comm : MonoBehaviour
{
    static public bool UseDAQ = true;
    public float MessageTime = 0.005f;
    private Process Proc;

    private void Start()
    {
        if(!Application.isEditor)
        {
            var args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-daq")
                {
                    UseDAQ = true;
                    break;
                }
            }
        }
        else
        {
            UseDAQ = false;
        }

        if (!UseDAQ) return;
        string daqPath;
        if (Application.isEditor)
        {
            string fullPath = Directory.GetCurrentDirectory();
            string parentDir = Directory.GetParent(fullPath).ToString();
            daqPath = parentDir + @"\DAQ-Writer.exe";
        }
        else
        {
            string fullpath = Directory.GetCurrentDirectory();
            daqPath = fullpath + @"\DAQ-Writer.exe";
        }

        Proc = new Process
        {
            StartInfo =
            {
                FileName = daqPath,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            }
        };
        try
        {
            bool procSuccess = Proc.Start();
        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
        }
    }

    public void SendMessageToDaq()
    {
        SendZero();
        Invoke("SendOne", MessageTime);
    }

    private void OnDisable()
    {
        if (!UseDAQ) return;
        Proc.StandardInput.WriteLine("2");
        Proc.Kill();
    }

    private void SendZero()
    {
        Proc.StandardInput.WriteLine("0");
    }

    private void SendOne()
    {
        Proc.StandardInput.WriteLine("1");
    }
}
