﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    private float ExitWaitTime;
    private float TimeLeftUntilExit;
    public bool IsPreparingToExit;

	private void Start ()
    {
        ExitWaitTime = 5.0f;
        TimeLeftUntilExit = 0.0f;
        IsPreparingToExit = false;

        var args = System.Environment.GetCommandLineArgs();
        string glove = "";
        for (var i = 0; i < args.Length; i++)
        {
            if (args[i] == "--glove")
            {
                glove = args[i + i];
                break;
            }
        }

        if(glove == "5dt")
        {
            // set up 5dt glove
        }
        else if(glove == "cyberglove")
        {
            // set up cyberglove
        }
    }
	
	private void Update ()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (!Application.isEditor)
                Application.Quit();
        }

        if (!IsPreparingToExit) return;
        TimeLeftUntilExit += Time.deltaTime;
        if (!(TimeLeftUntilExit > ExitWaitTime)) return;
        if (!Application.isEditor)
            Application.Quit();
    }
}
