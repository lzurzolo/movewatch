﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cylinder : MonoBehaviour
{
    public GameObject following;
    public GameObject cylinder;
    [Range(0.0f, 1.0f)]
    public float interested;

    private bool Visibility = true;

    private GameObject CurrentFollowing;

    private void Start()
    {
        CurrentFollowing = following;
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
    }

    private void Update()
    {
        cylinder.GetComponent<Renderer>().enabled = Visibility;
    }

    private void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, CurrentFollowing.transform.position, interested);
    }

    public void ApplyRotation(float degrees)
    {
        if (CompareTag("Left"))
        {
            transform.rotation = Quaternion.Euler(90.0f, degrees, 0.0f);
        }

        if (CompareTag("Right"))
        {
            transform.rotation = Quaternion.Euler(90.0f, -degrees, 0.0f);
        }
    }

    public void SetInvisible()
    {
        Visibility = false;
    }

    public void SetVisible()
    {
        Visibility = true;
    }
}
