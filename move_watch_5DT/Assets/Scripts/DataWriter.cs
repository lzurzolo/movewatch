﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataWriter : MonoBehaviour
{
    private string DataFileName;
    private string EventFileName;
    private float Timer;
    public StreamWriter DataStreamWriter;
    public StreamWriter EventStreamWriter;
    private long Milliseconds;

    static public bool UseLogger = true;

    // Use this for initialization
    private void Start ()
    {
        if (!UseLogger) return;
        string[] args = System.Environment.GetCommandLineArgs();
        string logName = "";
        for (var i = 0; i < args.Length; i++)
        {
            if (args[i] == "--logFileName")
            {
                logName = args[i + 1];
                break;
            }
        }

        Timer = 0.0f;
        if (Application.isEditor)
        {
            DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
        }
        else
        {
            DataFileName = string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            EventFileName = string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
        }

        DirectoryInfo di;
        if (Application.isEditor)
        {
            di = Directory.CreateDirectory("../log/");
        }
        else
        {
            di = Directory.CreateDirectory("log/");
        }

        // check if there are any log files in the log directory from today
        string[] dataFiles = Directory.GetFiles(di.FullName, Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);
        string[] eventFiles = Directory.GetFiles(di.FullName, Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);

        // get the count of the log files
        int dataCount = dataFiles.Length;
        int eventCount = eventFiles.Length;

        // if there are no data log files from today use the default name

        if (dataCount == 0)
        {
            DataFileName = Application.isEditor ? string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt") : string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
        }
        else // otherwise append the count to the end of the file name to prevent overwriting
        {
            DataFileName = Application.isEditor ? string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + dataCount + ").txt") : string.Format(logName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + dataCount + ").txt");
        }

        // if there are no event log files from today use the default name

        if (eventCount == 0)
        {
            EventFileName = Application.isEditor ? string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt") : string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
        }
        else // otherwise append the count to the end of the file name to prevent overwriting
        {
            EventFileName = Application.isEditor ? string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + eventCount + ").txt") : string.Format(logName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + eventCount + ").txt");
        }

        DataStreamWriter = new StreamWriter(di.FullName + DataFileName, true);
        EventStreamWriter = new StreamWriter(di.FullName + EventFileName, true);

        EventStreamWriter.Write("time,ms,trialState,trialHands,leftRayStartPointX,leftRayStartPointY,leftRayStartPointZ,leftRayEndPointX,leftRayEndPointY,leftRayEndPointZ,rightRayStartPointX,rightRayStartPointY," +
                                "rightRayStartPointZ,rightRayEndPointX,rightRayEndPointY,rightRayEndPointZ\n");

        DataStreamWriter.Write(
            "time,ms,hand,indexProximal,indexMedial,middleProximal,middleMedial,ringProximal,ringMedial,pinkyProximal,pinkyMedial\n");
    }

    private void OnDestroy()
    {
        if (!UseLogger) return;
        EventStreamWriter.Close();
        DataStreamWriter.Close();
    }

    public void LogData()
    {
        if (GloveManager5DT.DebugTestMode) return;
        System.TimeSpan timeSpan;
        Timer += Time.deltaTime;
        timeSpan = System.TimeSpan.FromSeconds(Timer);

        Milliseconds = timeSpan.Minutes * 60000 + timeSpan.Seconds * 1000 + timeSpan.Milliseconds;
        DataStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
        DataStreamWriter.Write(Milliseconds);
        DataStreamWriter.Write(",left");

        for (var i = 3; i < GloveManager5DT.MaxSensorSize; i += 3)
        {
            if (GloveManager5DT.LeftSensors[i] <= 0.1f)
            {
                GloveManager5DT.LeftSensors[i] = 0.1f;
            }
            DataStreamWriter.Write("," + -1 * (90 * GloveManager5DT.LeftSensors[i] - 10));
            DataStreamWriter.Write("," + -1 * (90 * GloveManager5DT.LeftSensors[i + 1] - 10));
        }

        DataStreamWriter.Write("\n");

        Timer += Time.deltaTime;
        timeSpan = System.TimeSpan.FromSeconds(Timer);

        Milliseconds = timeSpan.Minutes * 60000 + timeSpan.Seconds * 1000 + timeSpan.Milliseconds;
        DataStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
        DataStreamWriter.Write(Milliseconds);
        DataStreamWriter.Write(",right");

        for (var i = 3; i < GloveManager5DT.MaxSensorSize; i += 3)
        {
            if (GloveManager5DT.RightSensors[i] <= 0.1f)
            {
                GloveManager5DT.RightSensors[i] = 0.1f;
            }
            DataStreamWriter.Write("," + (90 * GloveManager5DT.RightSensors[i] - 10));
            DataStreamWriter.Write("," + (90 * GloveManager5DT.RightSensors[i + 1] - 10));
        }

        DataStreamWriter.Write("\n");
    }

    public void LogCustomData(string input)
    {
        EventStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
        EventStreamWriter.Write(Milliseconds);
        EventStreamWriter.Write("," + input);
        EventStreamWriter.Write(System.DateTime.Today.ToString("," + "MM/dd/yyyy"));
        EventStreamWriter.Write("\n");
    }
}
