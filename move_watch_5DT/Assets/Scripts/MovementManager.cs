﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;

public class MovementManager : MonoBehaviour
{
    [Header("Hand")]
    public Transform LeftIndexFingerPoint;
    public Transform LeftIndexFingerKnuckle;
    public Transform RightIndexFingerPoint;
    public Transform RightIndexFingerKnuckle;
    public GameObject LeftHand;
    public GameObject RightHand;
    public Transform LeftHandContainer;
    public Transform RightHandContainer;
    public GameObject DummyLeftHand;
    public GameObject DummyRightHand;
    public GameObject LeftHands;
    public GameObject RightHands;

    [Header("Ray")]
    public GameObject LeftRay;
    public GameObject RightRay;
    private Cylinder LeftRayScript;
    private Cylinder RightRayScript;
    public Transform LeftRayEndPoint;
    public Transform RightRayEndPoint;

    [Header("HUD")]
    public Text HUDMovementsLeft;
    public Text HUDCurrentMove;
    public Text HUDPrepareToMove;
    public Text HUDFilterLevelHeader;
    public Text HUDFilterLevel;

    private int NumberOfPromptsLeft;

    private Renderer LeftRayRenderer;
    private Renderer RightRayRenderer;
    private Renderer LeftFingerRayRenderer;
    private Renderer RightFingerRayRenderer;

    private List<Prompt> PromptList;
    private Prompt CurrentPrompt;
    private static short CurrentPromptIndex;

    private float CurrentTime;

    // Set this flag to test hand movement, this will prevent the simulation from running through movements
    public static bool HandDebugMode;
    private DataWriter Writer;
    private Comm DAQComm;

    [Header("Simulation Parameters")]
    public bool UseInvisibleMode = false;

    public enum HandOptions
    {
        ENABLE_BOTH,
        DISABLE_LEFT,
        DISABLE_RIGHT
    };

    public HandOptions SelectedHandOption;

    private float TotalPromptDuration;
    private float UndoMirrorDuration;
    private float RestAndPrepDuration;
    private bool AppliedMirroring;
    private bool UndidMirroring;
    private bool SentRestDAQMessage;
    private bool SentPrepDAQMessage;
    private bool SentMoveDAQMessage;
    private bool SentObserveDAQMessage;
    private bool LoggedRestPrompt;
    private bool LoggedPrepPrompt;
    private bool LoggedMovePrompt;
    private bool LoggedObservePrompt;
    private bool StartedObserveAnimation;
    private bool StartedExperiment;

    public Animation LeftHandAnimation;
    public Animation RightHandAnimation;

    void Start ()
    {
        Zero();
        ReadConfigFileToMovementList();
        CountNumberOfMovements();
        SetHUDNumberOfMovesLeft(NumberOfPromptsLeft);
        GetLoggingReference();
        GetDAQReference();
        GetRayReferences();
        ConfigureHands();
    }
	
	private void Update ()
    {
        if(Input.GetKeyDown("space"))
        {
            StartedExperiment = true;
            HUDFilterLevelHeader.enabled = false;
            HUDFilterLevel.enabled = false;
        }

        if (!StartedExperiment) return;
        CurrentTime += Time.deltaTime;

        switch (SelectedHandOption)
        {
            case HandOptions.DISABLE_LEFT:
                LeftRayScript.SetInvisible();
                DummyLeftHand.SetActive(true);
                LeftHand.SetActive(false);
                break;
            case HandOptions.DISABLE_RIGHT:
                RightRayScript.SetInvisible();
                DummyRightHand.SetActive(true);
                RightHand.SetActive(false);
                break;
        }

        NewPromptUpdate();
        RestUpdate();
        PrepareUpdate();
        if (!CurrentPrompt.Observe)
        {
            MoveUpdate();
        }
        else
        {
            ObserveUpdate();
        }

        if (DataWriter.UseLogger)
            Writer.LogData();
    }

    private void Zero()
    {
        CurrentPromptIndex = 0;
        NumberOfPromptsLeft = 0;
        CurrentTime = 0.0f;
        AppliedMirroring = false;
        UndidMirroring = false;
        UndoMirrorDuration = 0.0f;
        StartedExperiment = false;
        SentRestDAQMessage = false;
        SentPrepDAQMessage = false;
        SentMoveDAQMessage = false;
        SentObserveDAQMessage = false;
        LoggedRestPrompt = false;
        LoggedPrepPrompt = false;
        LoggedMovePrompt = false;
        LoggedObservePrompt = false;
        StartedObserveAnimation = false;
    }

    private void ReadConfigFileToMovementList()
    {
        string configFile = Path.Combine(Application.dataPath, "config.json");
        string movementData = File.ReadAllText(configFile);
        PromptList = JsonConvert.DeserializeObject<List<Prompt>>(movementData);

        foreach (var p in PromptList)
        {
            p.Init();
        }
    }

    private void CountNumberOfMovements()
    {
        NumberOfPromptsLeft = PromptList.Count;
    }

    private void SetHUDNumberOfMovesLeft(int movesLeft)
    {
        HUDMovementsLeft.text = movesLeft.ToString();
    }

    public void SetHUDCurrentMove(string currentMovement)
    {
        HUDCurrentMove.text = currentMovement;
    }

    private void GetLoggingReference()
    {
        if(DataWriter.UseLogger)
        {
            Writer = GameObject.Find("Data Writer").GetComponent<DataWriter>();
        }
    }

    private void GetDAQReference()
    {
        if(Comm.UseDAQ)
        {
            DAQComm = GameObject.Find("DAQComm").GetComponent<Comm>();
        }
    }

    private void GetRayReferences()
    {
        LeftRayScript = LeftRay.GetComponent<Cylinder>();
        RightRayScript = RightRay.GetComponent<Cylinder>();

        LeftRayRenderer = GameObject.Find("RightCylinder").GetComponent<Renderer>();
        RightRayRenderer = GameObject.Find("LeftCylinder").GetComponent<Renderer>();
    }

    private void NewPromptUpdate()
    {
        if (!(CurrentTime > TotalPromptDuration) || LeftHandAnimation.isPlaying || RightHandAnimation.isPlaying) return;
        if (CurrentPromptIndex < PromptList.Count)
        {
            StepToNextMovement();
            CurrentTime = 0.0f;
            SetHUDNumberOfMovesLeft(NumberOfPromptsLeft + 1);
            UnsetOneTimeSequenceFlags();
        }
        else
        {
            GameObject.Find("ApplicationManager").GetComponent<ApplicationManager>().IsPreparingToExit = true;
            HUDPrepareToMove.text = "Test Completed. Exiting...";
            HUDPrepareToMove.enabled = true;
            SetHUDCurrentMove("");
            LeftRayScript.SetInvisible();
            RightRayScript.SetInvisible();
            LeftFingerRayRenderer.enabled = false;
            RightFingerRayRenderer.enabled = false;
            SetHUDNumberOfMovesLeft(0);
        }
    }

    private void RestUpdate()
    {
        if (!(CurrentTime < CurrentPrompt.RestTime)) return;
        if(Comm.UseDAQ && !SentRestDAQMessage)
        {
            DAQComm.SendMessageToDaq();
            SentRestDAQMessage = true;
        }
            
        if(!LoggedRestPrompt)
        {
            Log("Rest");
            LoggedRestPrompt = true;
        }

        LeftRayScript.ApplyRotation(0.0f);
        RightRayScript.ApplyRotation(0.0f);
        LeftRayScript.SetVisible();
        RightRayScript.SetVisible();
        SetHUDCurrentMove("Rest");
        if (!CurrentPrompt.LastMirror || UndidMirroring || !(CurrentTime > UndoMirrorDuration)) return;
        if (CurrentPrompt.Mirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
        {
            // reserved
        }
        else
        {
            switch (CurrentPrompt.LastMirrorType)
            {
                case EMirrorType.E_LEFT:
                {
                    UndoMirroring(LeftHands, LeftHand, RightHands,
                        SelectedHandOption == HandOptions.DISABLE_RIGHT ? DummyRightHand : RightHand);

                    break;
                }
                case EMirrorType.E_RIGHT:
                {
                    UndoMirroring(RightHands, RightHand, LeftHands,
                        SelectedHandOption == HandOptions.DISABLE_LEFT ? DummyLeftHand : LeftHand);

                    break;
                }
            }
        }
        UndidMirroring = true;
    }

    private void PrepareUpdate()
    {
        if (!(CurrentTime > CurrentPrompt.RestTime) || !(CurrentTime < RestAndPrepDuration)) return;
        if (Comm.UseDAQ && !SentPrepDAQMessage)
        {
            DAQComm.SendMessageToDaq();
            SentPrepDAQMessage = true;
        }
            
        if(!LoggedPrepPrompt)
        {
            Log("Prep");
            LoggedPrepPrompt = true;
        }

        HUDPrepareToMove.enabled = true;
        if(CurrentPrompt.Mirror)
        {
            if (!CurrentPrompt.Observe)
            {
                HUDPrepareToMove.text = "Prepare to move " + CurrentPrompt.WhichHand + " (MIRROR)";
            }
            else
            {
                HUDPrepareToMove.text = "Prepare to observe " + CurrentPrompt.WhichHand + " (MIRROR)";
            }

        }
        else
        {
            if (!CurrentPrompt.Observe)
            {
                HUDPrepareToMove.text = "Prepare to move " + CurrentPrompt.WhichHand;
            }
            else
            {
                HUDPrepareToMove.text = "Prepare to observe " + CurrentPrompt.WhichHand;
            }
        }
    }

    private void MoveUpdate()
    {
        if (!(CurrentTime > RestAndPrepDuration) || !(CurrentTime < TotalPromptDuration)) return;
        if (Comm.UseDAQ && !SentMoveDAQMessage)
        {
            DAQComm.SendMessageToDaq();
            SentMoveDAQMessage = true;
        }
            
        if(!LoggedMovePrompt)
        {
            Log("Move");
            LoggedMovePrompt = true;
        }

        HUDPrepareToMove.enabled = false;
        HUDPrepareToMove.text = "";
        if(CurrentPrompt.Mirror)
        {
            SetHUDCurrentMove("Move " + CurrentPrompt.WhichHand + " (Mirror)");
        }
        else
        {
            SetHUDCurrentMove("Move " + CurrentPrompt.WhichHand);
        }

        switch (CurrentPrompt.WhichHand)
        {
            case "Left":
            {
                if(CurrentPrompt.Mirror)
                {
                    LeftRayScript.ApplyRotation(-CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }
                else
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }

                if(CurrentPrompt.Mirror && !AppliedMirroring)
                {
                    if (CurrentPrompt.LastMirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
                    {
                        // reserved
                    }
                    else
                    {
                        ApplyMirroring(LeftHands, LeftHand, RightHands,
                            SelectedHandOption == HandOptions.DISABLE_RIGHT ? DummyRightHand : RightHand);
                    }
                    AppliedMirroring = true;
                }

                RightRayScript.SetInvisible();
                LeftRayScript.SetVisible();
                break;
            }
            case "Right":
            {
                if (CurrentPrompt.Mirror)
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(-CurrentPrompt.Angle);
                }
                else
                {
                    LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                    RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                }
                if (CurrentPrompt.Mirror && !AppliedMirroring)
                {
                    if (CurrentPrompt.LastMirror && CurrentPrompt.MirrorType == CurrentPrompt.LastMirrorType)
                    {
                        // reserved for future use
                    }
                    else
                    {
                        if (SelectedHandOption == HandOptions.DISABLE_LEFT)
                        {
                            ApplyMirroring(RightHands, RightHand, LeftHands, DummyLeftHand);
                        }
                        else
                        {
                            ApplyMirroring(RightHands, RightHand, LeftHands, DummyLeftHand);
                        }

                    }
                    AppliedMirroring = true;
                }
                RightRayScript.SetVisible();
                LeftRayScript.SetInvisible();
                break;
            }
            default:
                RightRayScript.SetVisible();
                LeftRayScript.SetVisible();
                LeftRayScript.ApplyRotation(CurrentPrompt.Angle);
                RightRayScript.ApplyRotation(CurrentPrompt.Angle);
                break;
        }
    }

    private void ObserveUpdate()
    {
        if (!(CurrentTime > RestAndPrepDuration) || !(CurrentTime < TotalPromptDuration)) return;
        RightRayScript.SetInvisible();
        LeftRayScript.SetInvisible();

        if (Comm.UseDAQ && !SentObserveDAQMessage)
        {
            DAQComm.SendMessageToDaq();
            SentObserveDAQMessage = true;
        }

        if (!LoggedObservePrompt)
        {
            Log("Observe");
            LoggedObservePrompt = true;
        }

        HUDPrepareToMove.enabled = false;
        HUDPrepareToMove.text = "";
        if (CurrentPrompt.Mirror)
        {
            SetHUDCurrentMove("Observe " + CurrentPrompt.WhichHand + " (Mirror)");
        }
        else
        {
            SetHUDCurrentMove("Observe " + CurrentPrompt.WhichHand);
        }

        if (StartedObserveAnimation) return;
        switch (CurrentPrompt.WhichHand)
        {
            case "Left":
                LeftHandAnimation.Play("LeftHand");
                break;
            case "Right":
                RightHandAnimation.Play("RightHand");
                break;
            default:
                LeftHandAnimation.Play("LeftHand");
                RightHandAnimation.Play("RightHand");
                break;
        }
        StartedObserveAnimation = true;
    }

    private void StepToNextMovement()
    {
        CurrentPrompt = GetNextMovement();
        TotalPromptDuration = CurrentPrompt.RestTime + CurrentPrompt.PrepTime + CurrentPrompt.MoveTime;
        RestAndPrepDuration = CurrentPrompt.RestTime + CurrentPrompt.PrepTime;
        UndoMirrorDuration = CurrentPrompt.RestTime * 0.5f;
        CurrentPromptIndex++;
        NumberOfPromptsLeft--;
    }

    private Prompt GetNextMovement()
    {
        return PromptList[CurrentPromptIndex];
    }

    private void UnsetOneTimeSequenceFlags()
    {
        AppliedMirroring = false;
        UndidMirroring = false;

        SentRestDAQMessage = false;
        SentPrepDAQMessage = false;
        SentMoveDAQMessage = false;
        SentObserveDAQMessage = false;

        LoggedPrepPrompt = false;
        LoggedRestPrompt = false;
        LoggedMovePrompt = false;
        LoggedObservePrompt = false;

        StartedObserveAnimation = false;
    }

    private void Log(string prompt)
    {
        var leftIndexFingerKnucklePos = LeftIndexFingerKnuckle.position;
        var rightIndexFingerKnucklePos = RightIndexFingerKnuckle.position;
        var leftRayEndPointPos = LeftRayEndPoint.position;
        var rightRayEndPointPos = RightRayEndPoint.position;
        
        Writer.LogCustomData(
            prompt +
            "," +
            CurrentPrompt.WhichHand +
            "," +
            leftIndexFingerKnucklePos.x +
            "," +
            leftIndexFingerKnucklePos.y +
            "," +
            leftIndexFingerKnucklePos.z +
            "," +
            leftRayEndPointPos.x +
            "," +
            leftRayEndPointPos.y +
            "," +
            leftRayEndPointPos.z +
            "," +
            rightIndexFingerKnucklePos.x +
            "," +
            rightIndexFingerKnucklePos.y +
            "," +
            rightIndexFingerKnucklePos.z +
            "," +
            rightRayEndPointPos.x +
            "," +
            rightRayEndPointPos.y +
            "," +
            rightRayEndPointPos.z);
    }

    private void ConfigureHands()
    {
        if (!Application.isEditor)
        {
            var args = System.Environment.GetCommandLineArgs();
            string handOption = "";
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "--hands")
                {
                    handOption = args[i + 1];
                    break;
                }
            }

            switch (handOption)
            {
                case "enabled":
                    SelectedHandOption = HandOptions.ENABLE_BOTH;
                    DummyLeftHand.SetActive(false);
                    DummyRightHand.SetActive(false);
                    return;
                case "disableLeft":
                    DummyLeftHand.SetActive(true);
                    LeftHand.SetActive(false);
                    SelectedHandOption = HandOptions.DISABLE_LEFT;
                    break;
                case "disableRight":
                    DummyRightHand.SetActive(true);
                    RightHand.SetActive(false);
                    SelectedHandOption = HandOptions.DISABLE_RIGHT;
                    break;
            }
        }
        else
        {
            switch (SelectedHandOption)
            {
                case HandOptions.ENABLE_BOTH:
                    DummyLeftHand.SetActive(false);
                    DummyRightHand.SetActive(false);
                    return;
                case HandOptions.DISABLE_LEFT:
                    DummyLeftHand.SetActive(true);
                    LeftHand.SetActive(false);
                    break;
                case HandOptions.DISABLE_RIGHT:
                    DummyRightHand.SetActive(true);
                    RightHand.SetActive(false);
                    break;
            }
        }
    }

    private void ApplyMirroring(GameObject mirrorHands, GameObject mirrorHand, GameObject otherHands, GameObject otherHand)
    {
        Vector3 BufferedLeftHandPosition = LeftHandContainer.transform.position;
        Vector3 BufferedRightHandPosition = RightHandContainer.transform.position;

        Vector3 mirrorScale = new Vector3(-1.0f, -1.0f, -1.0f);
        mirrorHands.transform.localScale = mirrorScale;
        if (mirrorHands.CompareTag("Left"))
        {
            mirrorHands.transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
            otherHands.transform.localScale = new Vector3(-1.0f, -1.0f, -1.0f);
            otherHands.transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
        }
        else
        {
            mirrorHands.transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
            otherHands.transform.localScale = mirrorScale;
            otherHands.transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
        }

        LeftHandContainer.transform.position = BufferedRightHandPosition;
        RightHandContainer.transform.position = BufferedLeftHandPosition;
    }

    private void UndoMirroring(GameObject mirrorHands, GameObject mirrorHand, GameObject otherHands, GameObject otherHand)
    {
        Vector3 BufferedLeftHandPosition = LeftHandContainer.transform.position;
        Vector3 BufferedRightHandPosition = RightHandContainer.transform.position;

        Vector3 normalScale = new Vector3(1, 1, 1);
        mirrorHands.transform.localScale = normalScale;
        if (mirrorHands.CompareTag("Left"))
        {
            mirrorHands.transform.Rotate(0.0f, -180.0f, 0.0f, Space.Self);
            otherHands.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            otherHands.transform.Rotate(0.0f, -180.0f, 0.0f, Space.Self);
        }
        else
        {
            mirrorHands.transform.Rotate(0.0f, -180.0f, 0.0f, Space.Self);
            otherHands.transform.localScale = normalScale;
            otherHands.transform.Rotate(0.0f, -180.0f, 0.0f, Space.Self);
        }

        LeftHandContainer.transform.position = BufferedRightHandPosition;
        RightHandContainer.transform.position = BufferedLeftHandPosition;

        otherHand.SetActive(true);
        LeftRayScript.SetVisible();
        RightRayScript.SetVisible();
    }
}
