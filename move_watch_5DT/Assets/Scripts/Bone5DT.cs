﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bone5DT : MonoBehaviour
{
    public SensorEnum5DT ID;
    public float MultiplicationTerm;
    public float AdditionTerm;
    public int RotationInvert;

    private float LastZ;

    private delegate void TransformHand();
    private TransformHand TransformHandHandler;

    private Queue<float> FilterBuffer;
    private int FilterMaxBufferSize;

    private void Start()
    {
        if (GloveManager5DT.DebugTestMode) return;
        // determine the handedness of the owning game object
        string goName = gameObject.name;
        if (goName.StartsWith("L"))
        {
            TransformHandHandler = TransformLeftHand;
        }
        else if (goName.StartsWith("R"))
        {
            TransformHandHandler = TransformRightHand;
        }
        else
        {
            // TODO : error handling
        }

        if (GloveManager5DT.FilterLevel >= 1 && GloveManager5DT.FilterLevel <= 5)
        {
            FilterBuffer = new Queue<float>(GloveManager5DT.FilterLevel);
            FilterMaxBufferSize = GloveManager5DT.FilterLevel;
        }
        else
        {
            FilterBuffer = new Queue<float>(1);
            FilterMaxBufferSize = 1;
        }
    }

    private void Update()
    {
        if(!GloveManager5DT.DebugTestMode)
        {
            if (GloveManager5DT.FilterLevel != FilterMaxBufferSize)
            {
                AdjustBufferSize();
            }
            TransformHandHandler();
        }
	}

    private void TransformLeftHand()
    { 
        float r = MultiplicationTerm * GloveManager5DT.LeftSensors[(int)ID] - AdditionTerm;

        if(FilterBuffer.Count >= FilterMaxBufferSize)
        {
            FilterBuffer.Dequeue();
        }

        float newZ = r * RotationInvert;
        if(newZ > 0.0f)
        {
            newZ = 0.0f;
        }

        FilterBuffer.Enqueue(newZ);
        float average = FilterBuffer.ToArray().Sum() / FilterBuffer.Count;

        if(average > 0.0f)
        {
            average = 0.0f;
        }

        transform.localRotation = Quaternion.Euler(0, 0, average);
    }

    private void TransformRightHand()
    {
        float r = MultiplicationTerm * GloveManager5DT.RightSensors[(int)ID] - AdditionTerm;

        if (FilterBuffer.Count >= FilterMaxBufferSize)
        {
            FilterBuffer.Dequeue();
        }

        float newZ = r * RotationInvert;
        if (newZ < 0.0f)
        {
            newZ = 0.0f;
        }

        FilterBuffer.Enqueue(newZ);
        float average = FilterBuffer.ToArray().Sum() / FilterBuffer.Count;

        if(average < 0.0f)
        {
            average = 0.0f;
        }

        transform.localRotation = Quaternion.Euler(0, 0, average);
    }

    private void AdjustBufferSize()
    {
        int filterLevel = GloveManager5DT.FilterLevel;

        if(filterLevel < FilterMaxBufferSize)
        {
            int n = FilterMaxBufferSize - filterLevel;
            for(var i = 0; i < n; i++)
            {
                FilterBuffer.Dequeue();
            }
        }

        FilterMaxBufferSize = filterLevel;
    }
}
