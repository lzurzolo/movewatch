﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDTGloveUltraCSharpWrapper;
using System.IO;
using UnityEngine.UI;
using System;

public class GloveManager5DT : MonoBehaviour
{
    private CfdGlove RightGlove;
    private CfdGlove LeftGlove;

    public static float[] RightSensors;
    public static float[] LeftSensors;

    private INIParser Parser;
    public static int FilterLevel;
    public static bool DebugTestMode;
    public const int MaxSensorSize = 14;

    public Text HUDFilter;

	private void Start ()
    {
        Parser = new INIParser();
        string settingsFile = Path.Combine(Application.dataPath, "settings.ini");
        Parser.Open(settingsFile);

        int testMode = Parser.ReadValue("Glove Settings", "DebugTest", 0);
        DebugTestMode = Convert.ToBoolean(testMode);

        Debug.Log(DebugTestMode);

        // TODO : make usb port selection configurable
        if (DebugTestMode) return;
        CfdGlove tGlove = new CfdGlove();
        tGlove.Open("USB0");
        if (!tGlove.IsOpen())
        {
            Debug.Log("Failed to open glove at USB0");
        }
        else
        {
            if (tGlove.GetGloveHand() == (int)EGloveHand.FD_HAND_LEFT)
            {
                LeftGlove = tGlove;
                RightGlove = new CfdGlove();
                RightGlove.Open("USB1");
                if (!RightGlove.IsOpen())
                {
                    Debug.Log("Failed to open glove at USB1");
                }
            }
            else
            {
                RightGlove = tGlove;
                LeftGlove = new CfdGlove();
                LeftGlove.Open("USB1");
                if (!LeftGlove.IsOpen())
                {
                    Debug.Log("Failed to open glove at USB1");
                }
            }

            RightSensors = new float[MaxSensorSize];
            LeftSensors = new float[MaxSensorSize];

            FilterLevel = Parser.ReadValue("Glove Settings", "FilterStrength", 1);

            // ensure some idiot hasn't manually written an illegal value to the filter levels
            if (FilterLevel < 1)
                FilterLevel = 1;
            if (FilterLevel > 5)
                FilterLevel = 5;

            // write back to the ini for safety
            Parser.WriteValue("Glove Settings", "FilterStrength", FilterLevel);

            HUDFilter.text = FilterLevel.ToString();
        }
    }
	
	private void Update ()
    {
        if(!DebugTestMode)
        {
            RightGlove.GetSensorScaledAll(ref RightSensors);
            LeftGlove.GetSensorScaledAll(ref LeftSensors);
            
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (FilterLevel < 5)
                {
                    FilterLevel++;
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (FilterLevel > 1)
                {
                    FilterLevel--;
                }
            }
            
            HUDFilter.text = FilterLevel.ToString();
        }
    }

    private void OnDestroy()
    {
        if(!DebugTestMode)
        {
            Parser.WriteValue("Glove Settings", "FilterStrength", FilterLevel);
        }
        Parser.Close();
    }
}
