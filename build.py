import os, subprocess, sys

def main():
    currDir = os.getcwd()

    print("Looking for C# compiler...")
    csharpcompilers = findExecs("MSBuild.exe", "C:\\")
    if csharpcompilers.count == 0:
        print("Failed to find C# compiler. Please install Visual Studio 2017/2019 and rerun this script")
        sys.exit()

    print("C# compilers located. Checking for correct version...")
    foundCompiler = False
    for c in csharpcompilers:
        if "Visual Studio" in c:
            if "2017" or "2019" in c:
                compiler = c
                print("C# compiler located at " + c)
                foundCompiler = True
                break
    if foundCompiler == False:
        print("C# compile not installed. Please install Visual Studio 2017/2019 and rerun this script")
        sys.exit()

    configsln = currDir + "\\move_watch_configuration\\move_watch_configuration_forms.sln"
    subprocess.check_call([compiler, configsln, "/p:Configuration=Release", "/p:OutputPath=" + currDir])

    launchersln = currDir + "\\move_watch_launcher\\move_watch_launcher.sln"
    subprocess.check_call([compiler, launchersln, "/p:Configuration=Release", "/p:OutputPath=" + currDir])

    daqsdln = currDir + "\\daq-writer\\DAQ-Writer.sln"
    subprocess.check_call([compiler, daqsdln, "/p:Configuration=Release", "/p:OutputPath=" + currDir])

    print("Looking for Unity installations...")
    unityInstalls = findExecs("Unity.exe", "C:\\")
    if unityInstalls.count == 0:
        print("Failed to find a Unity installation. Please install Unity 2017 and rerun this script")
        sys.exit()

    print("Unity installation located. Checking for correct version...")
    foundUnity2017 = False
    for u in unityInstalls:
        if "2017" in u:
            unityInstall = u
            print("Unity 2017 installation located at " + u)
            foundUnity2017 = True
            break
    
    if foundUnity2017 == False:
        print("Unity 2017 not installed. Please install Unity 2017 and rerun this script")
        sys.exit()

    leapVRSrcDir = currDir + "\\move_watch_leap_vr"
    leapSrcDir = currDir + "\\move_watch_leap"
    gloveSrcDir = currDir + "\\move_watch_5DT"

    leapVROutputDir = currDir + "\\binvr\\"
    if not os.path.exists(leapVROutputDir):
        print("Creating Move Watch VR output directory...")
        os.mkdir(leapVROutputDir)
    leapVROutputExec = leapVROutputDir + "move_watch.exe"
    
    leapOutputDir = currDir + "\\bin\\"
    if not os.path.exists(leapOutputDir):
        print("Creating Move Watch Non-VR output directory...")
        os.mkdir(leapOutputDir)
    leapOutputExec = leapOutputDir + "move_watch.exe"

    gloveOutputDir = currDir + "\\binglove\\"
    if not os.path.exists(gloveOutputDir):
        print("Creating Move Watch Glove output directory...")
        os.mkdir(gloveOutputDir)
    gloveOutputExec = gloveOutputDir + "move_watch.exe"

    print("Building Move Watch VR...")
    success = subprocess.check_call([unityInstall, "-quit", "-batchmode", "-logFile", "stdoutvr.log", "-projectPath", leapVRSrcDir, "-buildWindows64Player", leapVROutputExec])
    if success != 0:
        print("Failed to build Move Watch VR, check log file stdoutvr.log for errors")
        sys.exit()
    print("Move Watch VR built successfully")

    print("Building Move Watch Non-VR...")
    success = subprocess.check_call([unityInstall, "-quit", "-batchmode", "-logFile", "stdout.log", "-projectPath", leapSrcDir, "-buildWindows64Player", leapOutputExec])
    if success != 0:
        print("Failed to build Move Watch Non-VR, check log file stdout.log for errors")
        sys.exit()
    print("Move Watch Non-VR built succesfully")

    print("Building Move Watch Glove...")
    success = subprocess.check_call([unityInstall, "-quit", "-batchmode", "-logFile", "stdoutglove.log", "-projectPath", gloveSrcDir, "-buildWindows64Player", gloveOutputExec])
    if success != 0:
        print("Failed to build Move Watch Glove, check log file stdoutglove.log for errors")
        sys.exit()
    print("Move Watch Glove built succesfully")

    print("Build succeeded. Exiting...")   

def findExecs(name, path):
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            result.append(os.path.join(root, name))
    return result

if __name__ == "__main__":
    main()